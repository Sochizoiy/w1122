﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSphere : MonoBehaviour
{
    public InputHandler inputHandler;
    void Update()
    {
        inputHandler.InputMaster = null;
        if (Input.GetKey("up"))
        {
            inputHandler.InputMaster = inputHandler.MoveUp;
        }
        if (Input.GetKey("down"))
        {
            inputHandler.InputMaster = inputHandler.MoveDown;
        }
        if (Input.GetKey("left"))
        {
            inputHandler.InputMaster = inputHandler.MoveLeft;
        }
        if (Input.GetKey("right"))
        {
            inputHandler.InputMaster = inputHandler.MoveRight;
        }
        if (Input.GetKeyDown("space"))
        {
            inputHandler.InputMaster = inputHandler.CreateSomething;
        }
        inputHandler.InputMaster?.Invoke();
    }
}
