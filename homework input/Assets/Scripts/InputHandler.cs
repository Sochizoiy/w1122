﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    [HideInInspector] public System.Action InputMaster;
    [SerializeField] private float _speed = 0;
    [SerializeField] private GameObject _gameobj = null;
    public void MoveLeft()
    { transform.position = transform.position + (Vector3.left * Time.deltaTime * _speed); }
    public void MoveRight()
    { transform.position = transform.position + (Vector3.right * Time.deltaTime * _speed); }
    public void MoveUp()
    { transform.position = transform.position + (Vector3.up * Time.deltaTime * _speed); }
    public void MoveDown()
    { transform.position = transform.position + (Vector3.up * -1 * Time.deltaTime * _speed); }
    public void CreateSomething()
    {
        GameObject asd = Instantiate(_gameobj, gameObject.transform.position, Quaternion.identity, null);
    }

}
